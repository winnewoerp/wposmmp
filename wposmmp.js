var wposmmpMap = '';

(function(){
	wposmmpMap = L.map("wposmmp-map").setView([53.55078,9.99310],12);
	var mapnikLayer = L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
		attribution: "Kartendaten &copy; <a href=\"https://www.openstreetmap.org/\">OpenStreetMap</a>-Mitwirkende, <a href=\"https://creativecommons.org/licenses/by-sa/2.0/\">CC-BY-SA</a>",
		maxZoom: 19
	}).addTo(wposmmpMap);
	var bounds = wposmmpMap.getBounds();
	document.querySelector('.query-form input[name="bounds"]').value = bounds.getSouth()+","+bounds.getWest()+","+bounds.getNorth()+","+bounds.getEast();
	wposmmpMap.on('zoomstart dragstart',function(){
		bounds = wposmmpMap.getBounds();
		document.querySelector('.query-form input[name="bounds"]').value = bounds.getSouth()+","+bounds.getWest()+","+bounds.getNorth()+","+bounds.getEast();
	});
	
	
	/*var selectedObjects = [];
	document.querySelector('.query-form').addEventListener('change', function(e){
		var elem = e.target;
		selectedObjects = [];
		document.querySelectorAll('.query-form input[type="checkbox"]:checked').forEach(function(elem) {
			selectedObjects.push(elem.value)
		});
		alert(selectedObjects);
	});*/
	
	
})();

function wposmmpMapsetBounds() {
	var bounds = wposmmpMap.getBounds();
	document.querySelector('.query-form input[name="bounds"]').value = bounds.getSouth()+","+bounds.getWest()+","+bounds.getNorth()+","+bounds.getEast();
}