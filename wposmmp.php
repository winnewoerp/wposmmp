<?php
/*
Plugin Name: WP OSM Meta Posts
Plugin URI: https://www.stadtkreation.de/en/wordpress-plugins-and-themes/
Description: Get selected ojects from the OpenStreetMap database to be automatically saved as WP post objects and add individual project meta data to the respective post objects. 
Version: 0.1
Author: STADTKREATION
Text Domain: wposmmp
Domain Path: /languages/
Author URI: https://www.stadtkreation.de/en/about-us/
*/

define('WPOSMMP_FILE', __FILE__ );
define('WPOSMMP_PATH', dirname( __FILE__ ) );
define('WPOSMMP_VERSION', '0.3' );


/* 
 * Enqueue scripts and styles
 */
function fsp2_terminbuchung_enqueue_styles() {
	wp_enqueue_style( 'wposmmp-leaflet-styles', plugins_url( '', WPOSMMP_FILE ).'/leaflet/leaflet.css', [], WPOSMMP_VERSION );
	wp_enqueue_script( 'wposmmp-leaflet', plugins_url( '', WPOSMMP_FILE ).'/leaflet/leaflet.js', [], WPOSMMP_VERSION, true );
	wp_enqueue_script( 'wposmmp-functions', plugins_url( '', WPOSMMP_FILE ).'/wposmmp.js', [], WPOSMMP_VERSION, true );
}
add_action('wp_enqueue_scripts','fsp2_terminbuchung_enqueue_styles');

function wposmmp_shortcode_query_form() {
	
	// output sample queries
	$output = '
	<div id="wposmmp-map" style="height:300px"></div>
	<p><a id="showBounds" href="#">Show bounds</a></p>
	<p>[out:json][timeout:25];(node["natural"="tree"](53.545076136998,10.006399154663,53.552011535109,10.017900466919);way["building"](53.545076136998,10.006399154663,53.552011535109,10.017900466919);relation["building"](53.545076136998,10.006399154663,53.552011535109,10.017900466919););out;>;out;</p>
<p>[out:json][timeout:25];relation["building"](53.545076136998,10.006399154663,53.552011535109,10.017900466919);node["natural"="tree"](53.545076136998,10.006399154663,53.552011535109,10.017900466919);way["building"](53.545076136998,10.006399154663,53.552011535109,10.017900466919);>;out;</p>
<p>[out:json][timeout:25];(way(659588322);node(w)->.z;way(30916745);node(w)->.z;);out;</p>
<p>[out:json][timeout:25];relation(222103);>;out;</p>';

	// array for object types and overpass snippets
	$object_types = array(
		'building' => 'way["building"]({{bbox}});relation["building"]({{bbox}});',
		'tree' => 'node["tree"]({{bbox}});',
		'shop' => 'node["shop"]({{bbox}});way["shop"]({{bbox}});relation["shop"]({{bbox}});',
	);	

	// handle form submission
	$query = '';
	$new_query = true;
	if($_POST['query-sent'] && $new_query) {
		$post_object_types = $_POST['object-type'];
		foreach($post_object_types as $post_object_type) {
			$query .= str_replace('{{bbox}}',$_POST['bounds'],$object_types[$post_object_type]);
		}
		
		// add query first and last parts
		$query = '[out:json][timeout:25];('.$query.');out;>;out;';
		
		echo $query;
		

		include('Overpass2Geojson/Overpass2Geojson.php');

		// get the path of the json file
		$filename = 'results.json';
		
		// compare old and current query
		$file_json = file_get_contents($filename);
		$file_object = json_decode($file_json);
		$old_query = $file_object->query;
		
		// if current query differs from old query
		if(trim($old_query) != $query) {
		
			// if file exists, get difference between file mod date and current time stamp
			if(file_get_contents($filename)) $time_difference = time()-filemtime($filename);
			
			if(file_get_contents($filename) && $time_difference<1) $output .= '<p>'.$time_difference.' - no new file has been written</p>';
			else {

				$overpass = 'http://overpass-api.de/api/interpreter?data='.$query;
				
				// collecting results in JSON format
				$json = false;
				@file_get_contents($overpass);
				$code=getHttpCode($http_response_header);
				if($code!=200) $output .= '<p style="color:red">Error code '.$code.'. Please check your Overpass query.</p>';
				else 
					{$json = file_get_contents($overpass);
				
					$json_decoded = json_decode($json);
					
					$json_decoded->query = strip_tags($_POST['overpass-query']);
					
					$json = json_encode($json_decoded);			
						
					$geojson = Overpass2Geojson::convertElements($json,true);

					$fp = fopen('results.json', 'w');
					fwrite($fp, $json);
					fclose($fp);
					$fp = fopen('results.geojson', 'w');
					fwrite($fp, $geojson);
					fclose($fp);

					$output .= '<p>'.$time_difference.' - a new file has been written</p>';
				}
			}
		}
		else {
			$output .= '<p style="color:orange">Query equals old query</p>';
			$new_query = false;
		}
	}
	
	// if form not submitted or current query equals old query
	if(!$_POST['query-sent'] || !$new_query) {
		$output .= '
<form class="query-form" method="post" action="">
	<p>Object types:<br />
		<label for="object-type-building"><input type="checkbox" name="object-type[]" value="building"> Buildings</label><br />
		<label for="object-type-tree"><input type="checkbox" name="object-type[]" value="tree"> Trees</label><br />
		<label for="object-type-tree"><input type="checkbox" name="object-type[]" value="shop"> Shops</label>
	</p>
	<!--<p>
		<label for="overpass-query">Modify your Overpass query<br />
			<input style="width:100%" id="overpass-query" type="text" cols="160" name="overpass-query" value="'.(isset($_POST['query']) ? strip_tags($_POST['query']) : '').'" />
		</label>
	</p>-->
	<p>
		<input type="submit" value="Create GeoJSON file based on my query" />
	</p>
	<input type="hidden" name="bounds" value="" />
	<input type="hidden" name="query-sent" value="true" />
</form>';
	}

	$files = array(
		'results.json',
		'results.geojson'
	);
	foreach($files as $file) {
		if(file_exists($file)) $output .= '<p><a href="'.$file.'">Open '.$file.'</a></p>';
	}

	return $output;
}
add_shortcode('query-form','wposmmp_shortcode_query_form');

function getHttpCode($http_response_header) {
	if(is_array($http_response_header))
	{
		$parts=explode(' ',$http_response_header[0]);
		if(count($parts)>1) //HTTP/1.0 <code> <text>
			return intval($parts[1]); //Get code
	}
	return 0;
}